--Main file

VERSION = "ALPHA 0.3 DEV"

Logger     = require 'horchata.logger'
Color      = require 'horchata.color'
Coll       = require 'horchata.collision'
Serializer = require 'horchata.serializer'

Splashy    = require 'splashy'

Player     = require 'lib.player'
Enemy      = require 'lib.enemy'
Bullet     = require 'lib.bullet'
Interface  = require 'lib.interface'
World      = require 'lib.world'

Menu       = require 'lib.mainmenu'
Options    = require 'lib.options'
Credits    = require 'lib.credits'

local opts = require 'opts'

local moonshine = require 'moonshine'

-- GLOBAL CONFIG

local music = nil
local gameover = nil
local iron_sprite = nil

local sc_screenwidth = 1920
local sc_screenheight = 1080

screenwidth  = nil 
screenheight = nil
scale_x = nil
scale_y = nil

gamestate = nil

music_tuned_on = opts.music
crt_turned_on = opts.retro

-----------------

--local bg_y  = 0
--local bg2_y = bg_y - imgheight
--local lifebar = (screenwidth - 20) / 100 * Player.life

-- WORLD VARS

-- Reserva
-- enemies  = {}
-- bullets  = {}
-- minerals = {
--     iron = {}
--     --cuprium = {},
--     --magnesium = {}
-- }

-- -- Añadidos al mundo
-- entities = {
--     bullets = {},
--     enemies = {},
--     minerals = {}
-- }
-----------------

function love.load()
    Logger:info("Iniciamos proceso de carga del juego.")
    
    -- Cargamos elementos estaticos y semiestáticos
    music = love.audio.newSource("resources/sounds/music.mp3", "stream")
    music:setVolume(0.4)
    gameover = love.audio.newSource("resources/sounds/gameover.mp3", "stream")
    gameover:setVolume(0.4)
    menu_screen = love.audio.newSource("resources/sounds/menu_screen.mp3", "stream")
    menu_screen:setVolume(0.4)

    font = love.graphics.newFont("resources/font.ttf", 25)
    love.graphics.setFont(font)
    
    iron_sprite = love.graphics.newImage("resources/iron_ore.png")   
    cursor = love.mouse.newCursor("resources/mouse.png", 0, 0)
    love.mouse.setVisible(false)

    Splashy.addSplash(love.graphics.newImage("resources/splash.png"))
    Splashy.addSplash(love.graphics.newImage("resources/splash-love.png"))
    Splashy.onComplete(function() gamestate = "menu" love.mouse.setVisible(true) end)

    
    screenwidth  = love.graphics.getWidth()
    screenheight = love.graphics.getHeight()

    scale_x = (sc_screenwidth/screenwidth)
    scale_y = (sc_screenheight/screenheight)

    Logger:info("Cargando enemigos y armas...")
    World:load()
    Logger:info(table.getn(World.enemies) .. " enemigos cargados.")
    Player:load()

    love.mouse.setCursor(cursor)
    Menu:load()
    Options:load()

    effect = moonshine(moonshine.effects.chromasep)
        .chain(moonshine.effects.crt)
        .chain(moonshine.effects.scanlines)
        .chain(moonshine.effects.glow)

    effect.chromasep.angle = 45
    effect.chromasep.radius = 4.5
    effect.glow.strength = 2
    effect.scanlines.width = 1.5
    effect.scanlines.opacity = 0.7
end

function love.update(dt)
    music_update()

    if gamestate == "maingame" then
        love.mouse.setVisible(false)
    elseif gamestate == "options" then
        love.mouse.setVisible(true)
        Options:update()
    elseif gamestate == "menu" then
        love.mouse.setVisible(true)
        Menu:update()
    else
        Splashy.update(dt)
    end

    if Player.life <= 0 then
        gamestate = "gameover"
    end

    if gamestate == "maingame" then
        World:update(dt)
        Player:update(dt)

        if love.keyboard.isDown("space") then
            Player:shot()
        end
    end
end

function love.draw()
    if crt_turned_on then
        effect(function()
            if gamestate == "maingame" then
                World:draw()
                Player:draw()
                Interface:draw()
            elseif gamestate == "options" then
                Options:draw()
            elseif gamestate == "credits" then
                Credits:draw()
            elseif gamestate == "menu" then
                Menu:draw()
            end
        end)
    else
        if gamestate == "maingame" then
            World:draw()
            Player:draw()
            Interface:draw()
        elseif gamestate == "options" then
            Options:draw()
        elseif gamestate == "credits" then
            Credits:draw()
        elseif gamestate == "menu" then
            Menu:draw()
        end
    end
        
    Splashy.draw()

    if gamestate ~= nil then
        love.graphics.print("AWESOME SPACE ROCKS SHOOTER " .. VERSION, 10, screenheight - 20, 0, 0.7, 0.7)
        love.graphics.print("A SOLFAMIDAS GAME", screenwidth - 188, screenheight - 20, 0, 0.7, 0.7)
    end

end

function reload()
    gamestate = "maingame"
    --Player.
end

function screen_reload()
    --Menu:load()
    --Options:load()
end

function repair(cost, health)
    if collectedMinerals > cost then
        if Player.life + health < 100 then
            Player.life = Player.life + health
        else
            Player.life = 100
        end

        collectedMinerals = collectedMinerals - cost
    end
end

-- Exit function
function love.keypressed(key, scancode, isrepeat)
    if key == "escape" then
        Splashy:skipAll()
        if gamestate ~= "menu" and gamestate ~= "credits" then
            if gamestate ~= nil then
                print("GMST: " .. gamestate)
                gamestate = "menu"
                World:restart()
                Player:restart()
                music:stop()
                --love.load()
            end
        else
            print("GMST: " .. gamestate)
            gamestate = "menu"
        end
    end

    if key == "m" then
        if gamestate == "maingame" then
            if music:getVolume() > 0 then
                music:setVolume(0)
            else 
                music:setVolume(0.4)
            end
        elseif gamestate == "gameover" then
            gameover:stop()
            World:restart()
            Player:restart()
            love.load()
        end
    end

    if key == "p" then
        local sound = love.audio.newSource("resources/sounds/pause.mp3", "static")
        sound:setVolume(0.4)        
        if gamestate == "maingame" then
            sound:play()
            gamestate = "paused"
        else
            gamestate = "maingame"
        end
    end

    if key == "r" then
        repair(20, 20)
    end
end

function love.mousemoved(x, y)
    Menu:mousemoved(x, y)
end

function music_update()
    if music_tuned_on then
        if gamestate == "maingame" then
            menu_screen:stop()
            music:play()
        elseif gamestate == "gameover" then
            music:stop()
            gameover:play()
        elseif gamestate == "paused" then
            music:pause()
        end
    else
        menu_screen:stop()
    end
end