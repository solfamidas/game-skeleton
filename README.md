# Awesome Space Rocks Shooter

![](https://img.itch.zone/aW1hZ2UvNTI1OTcxLzI3MzIyNDYucG5n/original/fm%2FA9h.png)

## Español

_(You can found the English version bellow)_

**Awesome Space Rocks Shooter** (ASRS) es un juego de disparos espaciales nacido de una serie de experimentos con Löve2D durante una semana complicada, como un hobby.

La mecánica es sencilla, como en cualquier otro shooter espacial: mueves la nave, disparas y destruyes los asteroides antes de que choquen contigo o con los escudos del planeta.

## English

_(Puedes encontrar la versión en español encima de esta)_

**Awesome Space Rocks Shooter** (ASRS) is a spacial shooter game, born from a series of experiments with Löve2D framework, during a complicated week, as a hobby.

The mechanics are simply, like in others spacial shooter: move the ship, shoot and destroy asteroids before they crash with your ship or the planet shields.