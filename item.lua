-- Clase Item
-- De esta clase heredan el resto de items del juego
-- con sus propiedades y sus funciones propias

local Base = require 'knife.base'
local Item = Base:extend({
    name = "",
    x = 0,
    y = 0
}) 

function Item:constructor(name, x, y)
    self.name = name
    self.x = x
    self.y = y
end

return Item