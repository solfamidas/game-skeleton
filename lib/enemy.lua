-- Subclase Enemy
-- Hereda de Entity

local Entity = require 'lib.entity'

-- Extiende la clase Item creando un nuevo objeto de tipo Item, clase Flask
-- con sus propiedades propias (no procedentes de Item)
local Enemy = Entity:extend({
    life = 3,
    vel  = love.graphics.getHeight() / 10,
    spr  = "",
    w    = 135,
    h    = 115
})

function Enemy:update(dt)
    self.y = self.y + self.vel * dt
end

function Enemy:damage(n)
    self.life = self.life - n
end

function Enemy:draw()
    love.graphics.draw(self.spr, self.x, self.y, 0, 0.2, 0.2)
end

return Enemy