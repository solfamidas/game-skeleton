-- Subclase Bullet
-- Hereda de Entity

local Entity = require 'lib.entity'

-- Extiende la clase Item creando un nuevo objeto de tipo Item, clase Flask
-- con sus propiedades propias (no procedentes de Item)
local Bullet = Entity:extend({
    dead = false
})

local sprite = love.graphics.newImage("resources/fire.png")

function Bullet:update(dt)
    if self.y > 0 then
        self.y = self.y - 25
    end
end

function Bullet:draw()
    if self.dead == false then
        --love.graphics.setColor(Color:RGBtoLove({255,0,0}))
        --love.graphics.circle("fill", self.x, self.y, 5, 10)
        love.graphics.draw(sprite, self.x, self.y, 0, 0.2, 0.2)
    end
end

return Bullet