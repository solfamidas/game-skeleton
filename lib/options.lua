local Options = {}

local Menu = require 'horchata.menu'
local opts = require 'opts'
-- menu

local music_status, crt_status

function Options:load()
    Menu:new("options")

    if opts.music then
        music_status = "ON"
    else
        music_status = "OFF"
    end

    if opts.retro then
        crt_status = "ON"
    else
        crt_status = "OFF"
    end

    Menu:addEntry(5, 15, 0, 0, false, nil, "OPTIONS")
    Menu:addEntry(5, 25, 80, 20, false, nil, "MUSIC")
    Menu:addEntry(15, 25, 80, 20, true, "switch_music", music_status)
    Menu:addEntry(5, 30, 80, 20, false, nil, "RETRO")
    Menu:addEntry(15, 30, 80, 20, true, "switch_crt", crt_status)
    Menu:addEntry(5, 80, 80, 20, true, "back", "BACK")
end

function Options:update()
    Menu:update()
end

function Options:draw()
    love.graphics.draw(World.bg, 0, bg_y, r, screenwidth/World.bg:getWidth(), screenheight/World.bg:getHeight())
    Menu:draw()
    --MUSIC VOLUME slider
    -- love.graphics.print("VOLUMEN", 120, (screenheight/2) + (60 / scale_y))
    -- love.graphics.rectangle("fill", 120, (screenheight/2) + (110 / scale_y), 300, 10)

    -- love.graphics.setColor(color)
    -- love.graphics.rectangle("fill", 125, (screenheight/2) + (100 / scale_y), 10, 30)
    -- love.graphics.setColor(r,g,b,a)
end

function save_options()

    local str = "opts = {\n"
    str = str .. "music = " .. tostring(music_tuned_on) .. ",\n"
    str = str .. "retro = " .. tostring(crt_turned_on) .. ",\n"
    str = str .. "}\n"
    str = str .. "return opts"

    local file = io.open("opts.lua", "w")
    io.output(file)
    io.write(str)
    io.close()
end

function MenuController(action)
    if action == "back" then

        save_options()
        gamestate = "menu"

    elseif action == "switch_music" then

        if music_tuned_on then
            Menu:updateLabel("switch_music", "OFF")
            music_tuned_on = false
        else
            Menu:updateLabel("switch_music", "ON")
            music_tuned_on = true
        end
    elseif action == "switch_crt" then
        if crt_turned_on then
            Menu:updateLabel("switch_crt", "OFF")
            crt_turned_on = false
        else
            Menu:updateLabel("switch_crt", "ON")
            crt_turned_on = true
        end
    end
end

return Options