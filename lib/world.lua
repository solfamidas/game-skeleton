local World = {
    bg = love.graphics.newImage("resources/stars.png"),

    enemies  = {},
    bullets  = {},
    minerals = {},

    entities = {
        bullets = {},
        enemies = {},
        minerals = {}
    }
}

-- TIMERS
local canGenerate = false
local canGenerateTimer = 0
----------------

local sprites = {
    love.graphics.newImage('resources/asteroid.png'),
    love.graphics.newImage("resources/asteroid2.png")
}

function World:load()
    for i = 1, 10, 1 do
        local enemy = Enemy()
        enemy.spr = sprites[love.math.random(1, table.getn(sprites))]
        table.insert(self.enemies, enemy)

        table.insert(self.bullets, Bullet())
    end

    for i = 1, 10, 1 do
        local iron_ore = {
            x = 0,
            y = 0,
            show = false
        }
        table.insert(self.minerals, iron_ore)
    end
end

function World:update(dt)

    if table.getn(self.entities.enemies) <= 8 and canGenerate then
        World:add_enemy()
    end

    if not canGenerate then
        canGenerateTimer = canGenerateTimer + (1 * dt)
        if canGenerateTimer >= 1.5 then
            canGenerate = true
            canGenerateTimer = 0
        end
    end

    -- Colision de minerales con la nave
    for k, v in ipairs(self.entities.minerals) do
        local mineral_player = Coll:detect(Player, v)
        -- if mineral_player.enter then
        --     if v.show then
        --         collectedMinerals = collectedMinerals + 1
        --     end

        --     v.show = false
        -- end
    end

    -- Colision de los asteroides con el planeta
    for k, v in ipairs(self.entities.enemies) do
        v:update(dt)

        if v.y > screenheight then
            Player:damage(2)
            table.insert(self.enemies, v)
            table.remove(self.entities.enemies, k)
        end
        
        local rock_player = Coll:detect(Player, v, 50, 20)
        if rock_player.enter then
            Player:damage(100)
        end
    end

    -- Colision de balas con los asteroides
    for b in pairs(self.entities.bullets) do
        local v = self.entities.bullets[b]

        v:update(dt)

        if v.y <= 0 then
            table.insert(self.bullets, v)
            table.remove(self.entities.bullets, b)
        end

        -- Colision de los asteroides con la nave
        for e in pairs(self.entities.enemies) do
            local collision = Coll:detect(self.entities.bullets[b], self.entities.enemies[e])
            if collision.enter then
                self.entities.enemies[e]:damage(1)

                table.insert(self.bullets, self.entities.bullets[b])
                table.remove(self.entities.bullets, i)

                if self.entities.enemies[e].life <= 0 then
                    Player.score = Player.score + 10
                    self.entities.enemies[e].life = 3

                    local ore = { x = 0, y = 0, show = true, w = 50, h = 50, rot = math.random(0, 2 * math.pi) }
                    ore.x = self.entities.enemies[e].x
                    ore.y = self.entities.enemies[e].y
                    table.insert(self.entities.minerals, ore)

                    table.insert(self.enemies, self.entities.enemies[e])
                    table.remove(self.entities.enemies, e)
                end
            end
        end
    end
end

function World:draw()
    love.graphics.setBackgroundColor(Color:RGBtoLove({24, 31, 45}))
    love.graphics.draw(self.bg, 0, bg_y, r, screenwidth/self.bg:getWidth(), screenheight/self.bg:getHeight())
    love.graphics.draw(self.bg, 0, bg2_y, r, screenwidth/self.bg:getWidth(), screenheight/self.bg:getHeight())

    for k, v in ipairs(self.entities.bullets) do
        v:draw()
    end

    for k, v in ipairs(self.entities.minerals) do
        -- if v.show then
        --     love.graphics.draw(iron_sprite, v.x, v.y, v.rot, scale_x, scale_y)
        -- end
    end

    for k, v in ipairs(self.entities.enemies) do
        v:draw()
    end
end

function World:add_enemy()
    asteroid = self.enemies[1]
    box_w = asteroid.w + 25
    box_h = asteroid.h + 20

    n_boxes = screenwidth/box_w

    asteroid.x = math.random(1, n_boxes) * box_w
    asteroid.y = - (math.random(1, 5) * box_h)

    if asteroid.x < (n_boxes * box_h) then
        table.insert(self.entities.enemies, asteroid)
        table.remove(self.enemies, 1)
        canGenerate = false
    end
end

function World:restart()
    self.enemies  = {}
    self.bullets  = {}
    self.minerals = {}

    self.entities.bullets = {}
    self.entities.enemies = {}
    self.entities.minerals = {}
    World:load()
end

return World