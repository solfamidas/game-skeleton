-- Clase Entity
-- De esta clase heredan el resto de entidades del juego
-- con sus propiedades y sus funciones propias

local Base = require 'knife.base'
local Entity = Base:extend({
    x = 0,
    y = 0,
    w = 0,
    h = 0
}) 

function Entity:constructor(x, y, w, h)
    self.x = x
    self.y = y
    self.w = w
    self.h = h
end

return Entity