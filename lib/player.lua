local Color = require 'horchata.color'

Player = {
    x = love.graphics.getWidth() / 2,
    y = 600,
    w = 72,
    h = 120,
    vel = 500,
    life = 100,
    collectedMinerals = 0,
    score = 0
}

local sprite = nil

local canShotTimer = 0.3
local canShot = true

function Player:damage(n)
    self.life = self.life - n
    if self.vel > 0 then
        self.vel = self.vel - (2 * n)
    end
end

function Player:shot()
    if canShot then
        shoot_fx = love.audio.newSource("resources/sounds/laser1.mp3", "static")
        shoot_fx:setVolume(0.9)
        shoot_fx:play()

        canShot = false

        bullet = World.bullets[1]
        bullet.x = Player.x + 30
        bullet.y = Player.y

        table.insert(World.entities.bullets, bullet)
        table.remove(World.bullets, 1)
    end
end

function Player:restart()
    self.x = love.graphics.getWidth() / 2
    self.y = 600
    self.w = 72
    self.h = 120
    self.vel = love.graphics.getWidth() / 2
    self.life = 100

    self.score = 0
    self.collectedMinerals = 0

    sprite = love.graphics.newImage("resources/ship.png")
end

function Player:load()
    sprite = love.graphics.newImage("resources/ship.png")
end

function Player:update(dt)
    if not canShot then
        canShotTimer = canShotTimer - (1 * dt)
        if canShotTimer < 0 then
            canShot = true
            canShotTimer = 0.3
        end
    end

    if love.keyboard.isDown("right") then
        Player.x = Player.x + Player.vel * dt
    elseif love.keyboard.isDown("left") then
        Player.x = Player.x - Player.vel * dt
    end

    if love.keyboard.isDown("up") then
        Player.y = Player.y - Player.vel * dt
    elseif love.keyboard.isDown("down") then
        Player.y = Player.y + Player.vel/2 * dt
    end
end

function Player:draw()
    love.graphics.draw(sprite, self.x, self.y, 0, 0.09, 0.09)
end

return Player