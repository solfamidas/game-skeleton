local menuengine = require "menuengine.menuengine"

local Menu = {}

menuengine.stop_on_nil_functions = false

-- Mainmenu
local mainmenu
local banner

-- Function to start after User selects something
local function mainmenu_finish(entrypoint)
    if entrypoint == 1 then
        gamestate = "maingame"
    elseif entrypoint == 2 then
        gamestate = "options"
    elseif entrypoint == 3 then
        gamestate = "credits"
    elseif entrypoint == 4 then
        love.event.quit()
    end
end

function Menu:mousemoved(x, y)
    menuengine.mousemoved(x, y)
end

function Menu:load()
    mainmenu = menuengine.new((screenwidth/2), (screenheight/2))
    mainmenu:addEntry("START", mainmenu_finish, 1, font, {1,1,1,nil}, Color:RGBtoLove({81, 185, 141}))  -- call "mainmenu_finish", args = "1"
    mainmenu:addEntry("OPTIONS", mainmenu_finish, 2, font, {1,1,1,nil}, Color:RGBtoLove({81, 185, 141}))  -- call "mainmenu_finish", args = "2"
    mainmenu:addEntry("CREDITS", mainmenu_finish, 3, font, {1,1,1,nil}, Color:RGBtoLove({81, 185, 141}))  -- call "mainmenu_finish", args = "2"
    mainmenu:addSep()
    mainmenu:addEntry("QUIT", mainmenu_finish, 4, font, {1,1,1,nil}, Color:RGBtoLove({81, 185, 141}))  -- call "mainmenu_finish", args = "3"

    banner = love.graphics.newImage("resources/game-banner.png")
end

function Menu:update()
    menu_screen:play()
    mainmenu:update()
end

function Menu:draw()
    love.graphics.setBackgroundColor(Color:RGBtoLove({24, 31, 45}))
    love.graphics.draw(World.bg, 0, bg_y, r, screenwidth/World.bg:getWidth(), screenheight/World.bg:getHeight())
    love.graphics.draw(banner, (screenwidth/3), (screenheight/6), 0, screenwidth/World.bg:getWidth(), screenheight/World.bg:getHeight())
    mainmenu:draw()
end

return Menu