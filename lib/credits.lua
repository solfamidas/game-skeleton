local Credits = {}

function Credits:load()
end

function Credits:update()
end

function Credits:draw()
    local credits_text = "AWESOME SPACE ROCKS SHOOTER\r\n\nA SOLFAMIDAS GAME\r\n\nCODE BY JUANJO SALVADOR\r\n\nART BY JUANJO SALVADOR\r\n\nMUSIC BY OBLIDIVM\r\n\nSPECIAL THANKS TO ALBERTO CORTES"
    love.graphics.print(credits_text, (screenwidth/2.7), (screenheight/3), 0, 1, 1)

    love.graphics.print("PRESS ESC TO BACK TO MAIN MENU", (screenwidth/2.7), (screenheight - 80), 0, 1, 1)
end

return Credits