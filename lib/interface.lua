local Interface = {}

local r,g,b,a = love.graphics.getColor()

local color = Color:RGBtoLove({81, 185, 141})

function Interface:draw()
    love.graphics.setColor(color)

    if gamestate == "maingame" then
        love.graphics.print("SCORE    " .. Player.score, 10, 10, 0, 1, 1)
        love.graphics.print("MINERALS " .. Player.collectedMinerals, 10, 35, 0, 1, 1)
        love.graphics.print("SHIELDS  " .. Player.life, screenwidth - 170, 10, 0, 1, 1)
    elseif gamestate == "gameover" then
        love.graphics.print("GAME OVER", (screenwidth/2) - 60, (screenheight/2) - 30, 0, 1, 1)
        love.graphics.print(Player.score, screenwidth/2, (screenheight/2), 0, 1, 1)

        love.graphics.print("PRESS ENTER TO RESTART", (screenwidth/2) - 150, (screenheight/2) + 80, 0, 1, 1)
    elseif gamestate == "paused" then
        love.graphics.print("GAME PAUSED", (screenwidth/2) - 60, (screenheight/2) - 30, 0, 1, 1)
        love.graphics.print("PRESS P TO RESUME", (screenwidth/2) - 100, (screenheight/2) + 80, 0, 1, 1)
    end

    love.graphics.setColor(r,g,b,a)
end

return Interface