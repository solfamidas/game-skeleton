local saved_opts = require 'opts'

function love.conf(t)
    t.window.title = "Awesome Space Rocks Shooter"
    t.window.width = saved_opts.width
    t.window.height = saved_opts.height
    t.window.fullscreen = saved_opts.fullscreen

    t.version = "11.3"
    t.console = true
end